{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Overview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook, you will be promptly guided through the process of creating practical exercises. Starting with Anaconda installation and environment creation, this overview will also advise you on structuring your Jupyter notebooks efficiently. Additionally, it will help with essential steps to prepare and upload your exercises to a Git repository, ensuring that you can easily deploy your exercises on [DOORS](https://doors-front.univ-grenoble-alpes.fr) to enhance your teaching experience."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Environment setup"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Anaconda installation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Anaconda is a popular, open-source distribution of the Python and R programming languages, widely used in scientific computing, data science, and machine learning. It aims to simplify package management and deployment by providing a large collection of pre-built packages through its package manager, Conda. One of Anaconda's key features is the ability to create isolated environments for different projects, ensuring independent management of dependencies and avoiding conflicts. To set up Anaconda and emulate a JupyterLab server, an interactive notebook environment, start by [downloading and installing Anaconda from its official website](https://www.anaconda.com/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![anaconda-environments](images/anaconda-environments.png \"Anaconda environments\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Environment creation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once installed, selecting the **\"Environments\"** tab in Anaconda Navigator will present you with an interface to manage your Conda environments, with the default (i.e., root) named **base**. Anaconda environments allow you to isolate different sets of Python packages and dependencies, ensuring that different projects can have their own unique setup without interference. In the main panel, the interface shows the packages installed in the selected environment, along with details like package names, versions, and the method of installation (Conda or pip)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Clicking on the **\"Create\"** button located at the bottom of the list of environments will open a dialog box where you can specify the details for your new environment. You will need to provide a name for the environment, ideally something that reflects the purpose or the project it is intended for (e.g., course-name-exam). Additionally, you have the option to select the version of Python (or R, if needed) for the environment. Although optional, creating a new environment for each project is highly recommended, as it maintains the project's dependencies separately. Be aware that loading an environment can take a few seconds."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### JupyterLab package installation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Following the creation of your new environment in Anaconda, the next step involves installing JupyterLab, which can be done in two ways: either through Anaconda Navigator's **\"Home\"** tab or via the command line. Either way, make sure to have selected the environment you wish to work on. Launching JupyterLab will automatically open itself in your web browser.\n",
    "\n",
    "- **Home**: First, select the new environment in the dropdown menu located in the top-left corner of the main area. Then, click on the **\"Install\"** button of JupyterLab's panel. Finally, after JupyterLab's installation, click on its panel's **\"Launch\"** button.\n",
    "\n",
    "- **Command line**: First, activate the new environment by selecting **\"Open Terminal\"** within Anaconda Navigator's **\"Environments\"**. Then, install JupyterLab's Conda package with `conda install -c conda-forge jupyterlab`. Finally, after JupyterLab's installation, launch it with the command `jupyter lab`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "| Home | Command line |\n",
    "| --- | --- |\n",
    "| ![anaconda-jupyterlab-install](images/anaconda-jupyterlab-install.png \"Anaconda install JupyterLab\") | ![anaconda-environment-start](images/anaconda-jupyterlab-install-cmd.png \"Anaconda environment start command line\") |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To install Conda packages or Python libraries in JupyterLab, in a JupyterLab's terminal, you can use the Conda package manager with commands like `conda install package_name` for Conda packages, or the pip package manager with `pip install package_name` for Python libraries not available through Conda. This process is straightforward and mirrors the package installation process in a standard Python environment. It's important to note that any package installed this way becomes part of the active Conda environment, thus maintaining the environment's integrity and dependencies specific to your project or task in JupyterLab."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Notebook creation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Notebook structure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Creating Jupyter notebooks for different classes with varied teaching goals involves tailoring the content and structure to meet the specific needs of each course. Jupyter notebooks are incredibly versatile, allowing for the integration of text, code, visualisations, and other multimedia elements, which makes them an excellent tool for educational purposes.\n",
    "\n",
    "When creating Jupyter notebooks for any class, it is crucial to focus on clarity and structure. The notebooks should be well-organised, presenting topics in a logical sequence that naturally progresses from basic to more complex concepts. This structured approach helps in comprehension and keeps students engaged. Engaging content is another essential element; incorporating interactive visualisations, real-world examples, and relatable scenarios can significantly enhance the learning experience. Additionally, it is important to ensure the notebooks are accessible to all students. This means using clear, jargon-free language, especially for beginners, and including explanations that are easy to understand, irrespective of the student's background in the subject. These general principles can help make Jupyter notebooks an effective and versatile tool for teaching a wide range of subjects."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Notebook size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Having multiple reasonably short notebooks instead of one long notebook significantly enhances readability and navigation. In a lengthy notebook, the content can become overwhelming, making it hard for readers to follow and comprehend the material. This is particularly challenging in educational or collaborative contexts where clarity is crucial. Short notebooks, with focused and digestible content, allow readers to easily understand and navigate through the material. They also facilitate a clearer logical structure, which is beneficial for both the creator and the audience of the notebook.\n",
    "\n",
    "To manage multiple notebooks effectively, it is a good practice to enumerate them at the beginning of the filename or to follow a consistent naming convention. This approach helps in understanding the sequence or hierarchy of the notebooks, ensuring that they are accessed and read in the intended order. For instance, naming your notebooks as “01_Data_Collection.ipynb”, “02_Data_Cleaning.ipynb”, “03_Data_Analysis.ipynb”, and so on, gives a clear indication of each notebook’s purpose and its place in the overall workflow."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deployment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dependency files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before uploading your practical exercises created in Anaconda to a Git repository, it is crucial to gather a list of all the dependencies, including packages and libraries, used in your environment. This step ensures that anyone who accesses your notebooks can replicate your environment and interact with the exercises as intended. To accomplish this, you need to create specific dependency files that list all the necessary components. These include a **requirements.txt** file for Python dependencies or an **environment.yml** file for Conda environments (can also include Python dependencies). These dependency files should be placed in a dedicated folder named **binder**, located at the root of your project folder."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```shell\n",
    "# Windows\n",
    "conda env export --no-builds | findstr -v \"prefix\" > environment.yml\n",
    "\n",
    "# Linux\n",
    "conda env export --no-builds | grep -v \"prefix\" > environment.yml\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Git upload"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After diligently testing your notebooks to confirm that they run smoothly without error, and having cleared all cell outputs for a clean presentation, the next crucial step is to establish a Git repository for your project. While it is advisable to use your institution's GitLab platform for consistency and potentially better integration with existing systems, this is not a strict requirement. Any Git platform can be used for this purpose.\n",
    "\n",
    "Begin by creating a blank project on the chosen Git platform. Once the project is set up, you will have to either use GitLab's online IDE to upload your project's files one-by-one, or [install Git from its official website](https://git-scm.com/downloads/) and follow the intructions below to upload the entire project to the Git platform. You may also use [a software with a graphical user interface client](https://git-scm.com/downloads/guis), such as [GitHub Desktop](https://desktop.github.com/), which may be recommended for beginners.\n",
    "\n",
    "Clone the Git repository to your local project folder using the URL provided by the Git platform. This action sets up a link between your local project and the remote repository. After cloning, add all your project files, including the Jupyter notebooks and any dependency files you've prepared, into this local Git repository. The final step involves committing these files to your local repository and then pushing them to the remote repository on the Git platform. The commit message may be **\"Initial commit\"**, if it is your first commit. This process effectively uploads your notebooks and their dependencies to the Git repository, making them accessible for version control, collaboration, or sharing with a wider audience."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```shell\n",
    "# Inside a terminal opened from your project's repository\n",
    "\n",
    "# Clone the Git repository\n",
    "git clone https://example.git ./\n",
    "\n",
    "# Add all files to commit\n",
    "git add *\n",
    "\n",
    "# Commit the changes with a message\n",
    "git commit -m \"Initial commit\"\n",
    "\n",
    "# Push the changes to the Git repository\n",
    "git push\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Integration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To initiate the construction of your new practical session on [DOORS](https://doors-front.univ-grenoble-alpes.fr), navigate to the **\"Practical Sessions\"** page accessible through your profile. Here, you will find a **\"+\"** button designed for adding new practical sessions. Clicking on this button allows you to start the process of integrating your project. You will be prompted to paste the URL of the Git repository of your project, as well as to enter the required information that describes or configures your practical session. The platform uses the configuration files you previously set up in the repository to create an image of your project. This image ensures that your practical session runs in an environment that replicates the one you used for development. While the platform builds this image, which might take some time, you have the option to monitor the process through build logs. These logs are particularly useful for identifying and troubleshooting any errors that may occur during the build, enabling you to make necessary adjustments to ensure a good setup. If any errors occure during the build process, you will be able to launch a rebuild through the practical session's **\"Advanced\"** settings.\n",
    "\n",
    "As the build of your new practical session progresses and even after its completion, you have the opportunity to refine various aspects of your session by adjusting its settings. Managing the access settings is a crucial part of this process. In order to allow access to the practical session, you need set a password that students will need to use for entry. This password is configured within the **\"Access\"** settings of the practical session. Here, you can also specify the start and end dates, defining the timeframe during which the session is available to your students."
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
