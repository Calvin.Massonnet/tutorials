{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Overview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook serves as a comprehensive guide to the interface and functionalities of JupyterLab, offering insights into interactive computing, data analysis, and code documentation. It covers the default JupyterLab layout and essential navigation techniques, along with fundamental actions like creating and formatting cells. Whether you are new to JupyterLab or seeking to master its features, this notebook provides valuable insights to help you navigate and use this interactive computing environment effectively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## JupyterLab's Interface"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "JupyterLab has a user-friendly interface designed to enhance the interactive computing experience. It provides a versatile environment for interactive computing and data analysis. Being also highly customisable, some sessions might have a different layout than the default, depending on the layout preference chosen for running practical sessions. However, here is a description of the default JupyterLab layout:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- **Main Area**: The central part of the interface is the main work area where you interact with notebooks, code consoles, and other JupyterLab documents.\n",
    "\n",
    "- **Left Sidebar**: The sidebar on the left contains the file browser, which allows you to navigate through your files and open existing notebooks or create new ones.\n",
    "\n",
    "- **Tabs**: JupyterLab uses tabs to organise open documents. Each open notebook or file appears as a tab at the top of the main work area. JupyterLab supports a drag-and-drop functionality, allowing you to arrange and organise your workspaces efficiently.\n",
    "\n",
    "- **Launcher**: The Launcher is accessible from the left sidebar and provides a quick way to create new notebooks, text files, terminals, and other JupyterLab documents.\n",
    "\n",
    "- **Menu Bar**: The menu bar at the top provides access to various commands and options, including file operations, editing, view settings, and more.\n",
    "\n",
    "- **Toolbar**: Beneath the menu bar, there is a toolbar with icons for common actions like save, cut, copy, paste, and run. The toolbar may vary depending on the type of document you are working on.\n",
    "\n",
    "- **Kernel Indicator**: In the right corner of the notebook, you will find the kernel indicator, showing the status of the kernel (e.g., busy, idle)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Navigation & Interaction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In JupyterLab, navigating the file browser and managing notebooks is intuitive. The file browser, located in the left sidebar, allows you to explore your file system and open notebooks. Click on folders to open it, and click on the breadcrumb above it to go to a parent folder. Alternatively, press the **\"Backspace\"** key to move back to the parent directory.\n",
    "\n",
    "Notebooks can be opened by double-clicking or using the **\"Open\"** option in the right-click context menu. Closing a notebook is achieved by clicking the **`x`** icon on the notebook's tab, and you can reopen it later through the file browser.\n",
    "\n",
    "The list of contents panel, situated within the left sidebar, provides an overview of open files, running terminals, and active notebooks. It offers quick access to open files and allows you to switch between them efficiently. Additionally, the panel displays running kernels, enabling you to monitor and manage active computational processes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below is a list of the main actions that you will mostly use in JupyterLab. These actions provide a quick and efficient way to navigate, manipulate, and interact with JupyterLab notebooks.\n",
    "\n",
    "- **Creating cells**: To create a new cell, use the **\"Insert\"** menu or the **B** key to add a cell below, or the **A** key to add one above.\n",
    "\n",
    "- **Changing cell format**: Switch between cell types (Code, Markdown, Raw) using the cell toolbar or keyboard shortcuts (**Y** for Code, **M** for Markdown, **R** for Raw).\n",
    "\n",
    "- **Running cells**: Execute a cell by clicking the **\"Run\"** button in the toolbar, pressing **Shift + Enter**, or selecting **\"Run Cells\"** from the menu.\n",
    "\n",
    "- **Copy and paste cells**: Copy a cell with **C**, cut with **X**, and paste below with **V**. These actions are accessible via the toolbar or **right-click** context menu.\n",
    "\n",
    "- **Moving cells**: Change cell order using the up (**K**) and down (**J**) arrow buttons, or drag cells to desired locations.\n",
    "\n",
    "- **Removing cells**: Delete a cell using the scissors icon in the toolbar, press **D** twice, or use the **\"Edit\"** menu.\n",
    "\n",
    "- **Saving the notebook**: Save the notebook with the floppy disk icon in the toolbar, **Ctrl + S** (Windows/Linux) or **Cmd + S** (Mac), or via the **\"File\"** menu.\n",
    "\n",
    "- **Cell operations**: Access additional cell operations (merge, split, etc.) through the toolbar or **\"Edit\"** menu.\n",
    "\n",
    "- **Command palette**: Open the command palette with **Ctrl + Shift + C** (Windows/Linux) or **Cmd + Shift + C** (Mac) to search and execute various commands."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Notebooks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In JupyterLab, a notebook is a digital document that allows you to create and share live code, equations, visualisations, and narrative text. It consists of a series of cells, each capable of containing **code**, **Markdown-formatted text**, or **raw content**. Notebooks provide an interactive and flexible environment for conducting data analysis, scientific research, and coding, enabling you to seamlessly combine executable code with explanatory text and visualisations in a single, interactive interface."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The order of running cells is significant due to the shared global state of the underlying Python kernel. Each cell in a notebook can modify or rely on variables, functions, or objects created in previous cells. When a cell is executed, it affects the overall state of the kernel, and subsequent cells can access or depend on this modified state. If cells were run independently of each other without considering their order, it could lead to inconsistent or unexpected results.\n",
    "\n",
    "For example, if a variable is defined in one cell and then used in a subsequent cell, running the second cell before the first one would result in an error because the variable hasn't been defined yet. Similarly, if cells perform computations or modify shared objects, running them out of order could lead to incorrect outcomes. The sequential execution ensures a logical flow of code and helps maintain a coherent state, allowing for the development of a step-by-step narrative in the notebook. It also facilitates the incremental development and debugging of code, as changes made in one cell immediately affect the subsequent cells. Therefore, **adhering to the order of cell execution is essential for the proper functioning and integrity of notebooks**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Code cells"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Code cells are used to write and execute code. You can write multiple lines of code within a single code cell. When you run the cell, the code is executed, and the output is displayed below the cell. Each code cell contains a block of code that can be executed with a modular and iterative approach. By breaking down a computational task into smaller, manageable cells, you can execute portions of your code individually, inspect intermediate results, and iteratively refine your algorithms. The output of a code cell, which may include printed text, visualisations, or error messages, is displayed directly below the cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Text following the character \"#\" is considered to be commented text instead of code in Python.\n",
    "# Comments are used to explain the surrounding code with words.\n",
    "\n",
    "\"\"\"\n",
    "    This is a multiple line comment in Python.\n",
    "    It is usually used to document a function.\n",
    "\"\"\"\n",
    "\n",
    "variable = 10\n",
    "\n",
    "number_list = [1, 2, 3, 4, 5]\n",
    "\n",
    "def multiply_function(list, multiplier):\n",
    "    \"\"\"\n",
    "        :param list: list of intergers\n",
    "        :param multiplier: integer\n",
    "    \"\"\"\n",
    "    for i in list:\n",
    "        print(i * multiplier)\n",
    "\n",
    "multiply_function(number_list, variable)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Markdown cells"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Markdown cells offer a versatile means to incorporate formatted text using Markdown syntax. These cells are particularly employed for creating headings and annotating code through documentation and explanations. The Markdown content within these cells can include various formatting elements such as **headers**, **lists**, **links**, and **emphasis**, providing a rich set of tools for expressive text representation. When a Markdown cell is executed, the Markdown content is rendered, displaying the formatted text as intended.\n",
    "\n",
    "Here is a link to a cheat sheet, gathering all basic Markdown syntax: <https://www.markdownguide.org/cheat-sheet/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This markdown cell will get formatted once run.\n",
    "\n",
    "Some *italic text* and some **bold text**.\n",
    "\n",
    "List of items:\n",
    "\n",
    "- An item\n",
    "- Another item\n",
    "    - A sub-item\n",
    "\n",
    "Ordered list of items:\n",
    "\n",
    "1. First item\n",
    "2. Second item\n",
    "3. Third item\n",
    "\n",
    "Table example:\n",
    "\n",
    "| Id | Name | Group |\n",
    "| --- | --- | --- |\n",
    "| 0 | Alice | Student |\n",
    "| 1 | Bob | Teacher |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Raw cells"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Raw cells provide a straightforward way to input content without any processing; the content remains unchanged, exactly as it is typed. These cells are valuable for incorporating text or data that shouldn't undergo code execution or Markdown rendering. Raw cells essentially preserve the raw, unaltered form of the input, making them suitable for including plain text, specialised formatting, or data representations that don't conform to code or Markdown conventions."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "This raw cell will be displayed as is and will not receive any specific formatting."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "doors-tutorials",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
